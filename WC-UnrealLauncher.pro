#-------------------------------------------------
#
# Project created by QtCreator 2015-03-20T19:01:54
#
#-------------------------------------------------

QT       += core gui network webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = WC-UnrealLauncher
TEMPLATE = app


SOURCES += \
    src/Launcher.cpp \
    src/OutputLog.cpp \
    src/AppEntry.cpp \
    src/NewsHandler.cpp

HEADERS  += \
    src/OutputLog.h \
    src/Launcher.h \
    src/NewsHandler.h

INCLUDEPATH += "src/"

FORMS    += launcher.ui

RESOURCES += \
    template/resource/resource.qrc
