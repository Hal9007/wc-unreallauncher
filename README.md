﻿# **Overview**

This is a standalone, Qt-based launcher for the [WesterosCraftUnreal](https://bitbucket.org/Hal9007/westeroscraftunreal) project. It handles client registration and authorization for logging into the WCU dedicated server.

This launcher is built for the purposes of the [WesterosCraft](http://www.westeroscraft.com) server.

![ScreenShot](http://i.imgur.com/lbaSbru.png)

# **License**
### WC-UnrealLauncher License ###

WC-UnrealLauncher copyright (c) 2015, WesterosCraft.
All rights reserved.

Redistribution of this software, both in source and binary forms, is NOT permitted without the prior written consent of the WesterosCraft server -  <westeroscraft@gmail.com>.