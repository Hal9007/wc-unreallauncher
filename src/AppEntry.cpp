#include "Launcher.h"
#include "ui_launcher.h"
#include "OutputLog.h"
#include "NewsHandler.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Launcher w;
    OutputLog *o = w.ui->outputLog;
    NewsHandler *n = new NewsHandler(o, w.ui->newsView);

    o->logMessage("Finished setting up launcher object.");

    // Hackish-ly force progress bar color change on Windows OS.
#ifdef WIN32
    w.ui->progressCover->setStyleSheet("QLabel { background-color: rgba(0, 0, 200, 150) }");
    o->logWarning("Overwriting stylesheet for launcher progress bar.");
#else
    w->ui->progressCover->setStyleSheet("QLabel { background-color: rgba(255, 255, 255, 0) }");
#endif

    // Connect internal signals.
    QObject::connect(w.ui->bCopy,SIGNAL(clicked()),w.ui->outputLog,SLOT(copyAll()));
    QObject::connect(w.ui->bClear,SIGNAL(clicked()),w.ui->outputLog,SLOT(clear()));

    o->logMessage("Internal signals successfully connected.");

    int percentage = 60;

    w.ui->progressBar->setValue(percentage);
    w.ui->progressCover->resize(((percentage * 700) / 100), w.ui->progressCover->height());

    o->logError("The reactor core is above 60 percent!");

    w.show();

    QPixmap p = n->newsImages[1];
    w.ui->newsView->setPixmap(p);

    return a.exec();
}
