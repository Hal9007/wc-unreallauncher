#include "OutputLog.h"
#include <sys/time.h>
#include <QScrollBar>

OutputLog::OutputLog(QWidget *parent) : QPlainTextEdit(parent)
{
    setReadOnly(true);
    setLineWrapMode(NoWrap);
    setUndoRedoEnabled(false);
}

// The millisecond-precision time prepended to each log message.
QString OutputLog::getTime()
{
    timeval now;
    gettimeofday(&now, NULL);
    int milli = now.tv_usec / 1000;

    char buffer [40];
    strftime(buffer, 40, "[%H:%M:%S", localtime(&now.tv_sec));
    char ctime[44] = "";
    sprintf(ctime, "%s.%03d]", buffer, milli);

    QString time = QString(QLatin1String(ctime));
    return time;
}

// Logs a message to the output log.
void OutputLog::logMessage(QString text)
{
    appendHtml(getTime() + " " + text);
}

// Logs a warning to the output log.
void OutputLog::logWarning(QString text)
{
    appendHtml(getTime() + tr("<font color = blue> WARNING: </font color>") + text);
}

// Logs an error to the output log.
void OutputLog::logError(QString text)
{
    appendHtml(getTime() + tr("<font color = maroon> ERROR: </font color>") + text);
}

void OutputLog::dumpLog()
{
    return;
}

// Copies all of the plaintext currently within the output log.
void OutputLog::copyAll()
{
    selectAll();
    copy();

    QTextCursor cursor = textCursor();
    cursor.clearSelection();
    setTextCursor(cursor);

    verticalScrollBar()->setValue(verticalScrollBar()->maximum());
    horizontalScrollBar()->setValue(0);
}
