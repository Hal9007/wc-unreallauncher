#include "NewsHandler.h"
#include "OutputLog.h"
#include <QNetworkAccessManager>
#include <QByteArray>

void NewsHandler::netManagerFinished(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError)
    {
        log->logError("Unable to fetch recent news. Please check your network connection.");
        return;
    }

    QByteArray imageData = reply->readAll();
    QPixmap pixmap;
    pixmap.loadFromData(imageData);
    newsImages[loadCounter] = pixmap;
}

NewsHandler::NewsHandler(OutputLog *o, QLabel *l)
{
    log = o;
    label = l;
    loadImages();
}

void NewsHandler::loadImages()
{
    QNetworkAccessManager *netManager = new QNetworkAccessManager(this);
    connect(netManager,SIGNAL(finished(QNetworkReply*)),this,SLOT(netManagerFinished(QNetworkReply*)));

    QUrl loadURL;
    for(loadCounter = 0; loadCounter < 4; ++loadCounter)
    {
        switch(loadCounter)
        {
            case 0:
                loadURL = "http://www.westeroscraft.x10host.com/images/news0.jpg";
                break;
            case 1:
                loadURL = "http://www.westeroscraft.x10host.com/images/news1.jpg";
                break;
            case 2:
                loadURL = "http://www.westeroscraft.x10host.com/images/news2.jpg";
                break;
            case 3:
                loadURL = "http://www.westeroscraft.x10host.com/images/news3.jpg";
                break;
            default:
                loadURL = "http://www.westeroscraft.x10host.com/images/error.jpg";
                break;
        }
        QNetworkRequest request(loadURL);
        netManager->get(request);
    }
    loadCounter = 0;
}
