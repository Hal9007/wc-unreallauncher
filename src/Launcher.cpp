#include "Launcher.h"
#include "ui_Launcher.h"
#include <QDesktopWidget>
#include <QDesktopServices>
#include <QSignalMapper>
#include <QtNetwork/QNetworkAccessManager>
#include <QUrl>

// Launcher slot to open the given URL.
void Launcher::openURL(QString link)
{
    QDesktopServices::openUrl(link);
}

// The constructor for the launcher window.
Launcher::Launcher(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Launcher)
{
    ui->setupUi(this);
    setWindowFlags(Qt::FramelessWindowHint);
    setAttribute(Qt::WA_TranslucentBackground);

    // Root launcher button signals.
    QSignalMapper* signalMapper = new QSignalMapper(this);
    connect(ui->bWebsite,SIGNAL(clicked()),signalMapper,SLOT(map()));

    signalMapper->setMapping(ui->bWebsite, "http://www.westeroscraft.com");

    connect(ui->bMinimize,SIGNAL(clicked()),this,SLOT(showMinimized()));
    connect(ui->bClose,SIGNAL(clicked()),this,SLOT(close()));
    connect(signalMapper, SIGNAL(mapped(QString)),this,SLOT(openURL(QString)));
}

// Destructor for the launcher window.
Launcher::~Launcher()
{
    delete ui;
}

// Overwritten showEvent for the launcher - center it on the screen.
void Launcher::showEvent(QShowEvent *event)
{
    Q_UNUSED(event);
    centerOnScreen();
}

void Launcher::centerOnScreen()
{
    QDesktopWidget screen;

    QRect screenGeom = screen.screenGeometry(this);

    int screenCenterX = screenGeom.center().x();
    int screenCenterY = screenGeom.center().y();

    move(screenCenterX - width () / 2,
         screenCenterY - height() / 2);
}
