#ifndef OUTPUTLOG_H
#define OUTPUTLOG_H

#include <QPlainTextEdit>

class OutputLog : public QPlainTextEdit
{
    Q_OBJECT

public:
    explicit OutputLog(QWidget *parent = 0);

    void logMessage(QString text);
    void logWarning(QString text);
    void logError(QString text);

private:
    QString getTime();
    bool checkLogs();
    void removeLog();

private slots:
    void dumpLog();
    void copyAll();
};

#endif // OUTPUTLOG_H
