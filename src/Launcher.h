#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <QMainWindow>
#include <QtNetwork/QNetworkReply>
#include <QPixmap>

namespace Ui
{
    class Launcher;
}

class Launcher : public QMainWindow
{
    Q_OBJECT

public:
    explicit Launcher(QWidget *parent = 0);
    Ui::Launcher *ui;
    ~Launcher();

protected:
    void showEvent(QShowEvent *event);

private:
    void centerOnScreen();
    QPixmap newsImages[5];

private slots:
    void openURL(QString link);
};

#endif // LAUNCHER_H
