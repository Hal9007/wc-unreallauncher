#ifndef NEWSHANDLER_H
#define NEWSHANDLER_H

#include "OutputLog.h"
#include <QLabel>
#include <QPixmap>
#include <QNetworkReply>

class NewsHandler : public QObject
{
    Q_OBJECT

public:
    NewsHandler(OutputLog *o, QLabel *l);
    QPixmap newsImages[4];

private:
    OutputLog *log;
    QLabel *label;
    void loadImages();
    int loadCounter;

private slots:
    void netManagerFinished(QNetworkReply *reply);
};

#endif // NEWSVIEW_H

